package web

import (
	"gitlab.com/mathisf/mathisfaiv.re/web/handlers"
	"gitlab.com/mathisf/mathisfaiv.re/web/middlewares"
	"html/template"
	"net/http"
)

func InitRouter(indexTemplate, resumeTemplate *template.Template) *http.ServeMux {
	staticFileServer := http.StripPrefix("/", http.FileServer(staticFiles()))

	routes := map[string]http.HandlerFunc{
		"/index":       func(w http.ResponseWriter, r *http.Request) { handlers.IndexHandler(w, r, indexTemplate) },
		"/index.html":  func(w http.ResponseWriter, r *http.Request) { handlers.IndexHandler(w, r, indexTemplate) },
		"/resume":      func(w http.ResponseWriter, r *http.Request) { handlers.ResumeHandler(w, r, resumeTemplate) },
		"/resume.html": func(w http.ResponseWriter, r *http.Request) { handlers.ResumeHandler(w, r, resumeTemplate) },
		"/resume.pdf":  func(w http.ResponseWriter, r *http.Request) { handlers.ResumePDFHandler(w, r) },

		"/en/index":       func(w http.ResponseWriter, r *http.Request) { handlers.IndexHandler(w, r, indexTemplate) },
		"/en/index.html":  func(w http.ResponseWriter, r *http.Request) { handlers.IndexHandler(w, r, indexTemplate) },
		"/en/resume":      func(w http.ResponseWriter, r *http.Request) { handlers.ResumeHandler(w, r, resumeTemplate) },
		"/en/resume.html": func(w http.ResponseWriter, r *http.Request) { handlers.ResumeHandler(w, r, resumeTemplate) },
		"/en/resume.pdf":  func(w http.ResponseWriter, r *http.Request) { handlers.ResumePDFHandler(w, r) },

		"/fr/index":       func(w http.ResponseWriter, r *http.Request) { handlers.IndexHandler(w, r, indexTemplate) },
		"/fr/index.html":  func(w http.ResponseWriter, r *http.Request) { handlers.IndexHandler(w, r, indexTemplate) },
		"/fr/resume":      func(w http.ResponseWriter, r *http.Request) { handlers.ResumeHandler(w, r, resumeTemplate) },
		"/fr/resume.html": func(w http.ResponseWriter, r *http.Request) { handlers.ResumeHandler(w, r, resumeTemplate) },
		"/fr/resume.pdf":  func(w http.ResponseWriter, r *http.Request) { handlers.ResumePDFHandler(w, r) },
	}

	mux := http.NewServeMux()

	for route, handler := range routes {
		mux.Handle(route, middlewares.RecoveryMiddleware(middlewares.LoggingMiddleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			handler(w, r)
		}))))
	}

	indexHandler := middlewares.RecoveryMiddleware(middlewares.LoggingMiddleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlers.IndexHandler(w, r, indexTemplate)
	})))

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" || r.URL.Path == "/en" || r.URL.Path == "/fr" || r.URL.Path == "/fr/" || r.URL.Path == "/en/" {
			indexHandler.ServeHTTP(w, r)
		} else {
			middlewares.RecoveryMiddleware(staticFileServer).ServeHTTP(w, r)
		}
	})

	http.Handle("/", mux)

	return mux
}
