let amountScrolled = 0,
  isBlurred = false;
(bgblurred = document.getElementById('background-image-blurred')),
  (arrow = document.getElementById('arrow')),
  (switchbutton = document.getElementById('switch-background-button')),
  (hgroup = document.getElementById('hgroup')),
  (previousIndex = 0),
  (width =
    (window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth) > 1300);
numberOfBackImages = 22;

let randIndex = Math.floor(Math.random() * numberOfBackImages);

function removeActive() {
  document.getElementById('tab-item-about').classList.remove('active');
  document.getElementById('tab-item-resume').classList.remove('active');
  document.getElementById('tab-item-contact').classList.remove('active');
  document.getElementById('about-section').classList.remove('active');
  document.getElementById('resume-section').classList.remove('active');
  document.getElementById('contact-section').classList.remove('active');
}

function getAnchor() {
  var currentUrl = document.URL,
    urlParts = currentUrl.split('#');
  return urlParts.length > 1 ? urlParts[1] : null;
}

function throttle(fn, wait) {
  var time = Date.now();
  return function () {
    if (time + wait - Date.now() < 0) {
      fn();
      time = Date.now();
    }
  };
}

function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

function getAmountScrolled() {
  amountScrolled = window.pageYOffset;
}

function toggleArrowSwitchButton(show) {
  if (show) {
    setTimeout(function () {
      arrow.classList.remove('hide');
      switchbutton.classList.remove('hide');
    }, 1);
  } else {
    setTimeout(function () {
      switchbutton.classList.add('hide');
      arrow.classList.add('hide');
    }, 1);
  }
}

function parallaxBlur() {
  throttle(getAmountScrolled(), 10);
  if (amountScrolled < 300) {
    if (amountScrolled >= 250) {
      bgblurred.style.opacity = 1;
      toggleArrowSwitchButton(false);
      isBlurred = true;
    } else if (amountScrolled < 100) {
      bgblurred.style.opacity = 0;
      toggleArrowSwitchButton(true);
      isBlurred = false;
    }
  } else {
    if (!isBlurred) {
      bgblurred.style.opacity = 1;
      toggleArrowSwitchButton(false);
      isBlurred = true;
    }
  }
}

function switchBackground(easterEgg) {
  randIndex = Math.floor(Math.random() * numberOfBackImages) + 1;
  while (randIndex == previousIndex) {
    randIndex = Math.floor(Math.random() * numberOfBackImages) + 1;
  }
  previousIndex = randIndex;
  if (Math.floor(Math.random() * 100) === 99 && easterEgg) {
    document.getElementById('background-image').style.background =
      'url(/images/coucou.jpg) no-repeat center center scroll';
    document.getElementById('background-image-blurred').style.background =
      'url(/images/coucou-blur.jpg) no-repeat center center scroll';
    document.getElementById(
      'background-image-blurred-bottom',
    ).style.background =
      'url(/images/coucou-blur.jpg) no-repeat center center scroll';
    document.getElementById('hgroup').style.background =
      'url(/images/coucou-blur.jpg) no-repeat center center scroll';
  } else {
    document.getElementById('background-image').style.background =
      'url(/images/' +
      (width ? '1080/back' : 'back') +
      randIndex +
      '.jpg) no-repeat center center scroll';
    document.getElementById('background-image-blurred').style.background =
      'url(/images/' +
      (width ? '1080/back' : 'back') +
      randIndex +
      '-blur.jpg) no-repeat center center scroll';
    document.getElementById(
      'background-image-blurred-bottom',
    ).style.background =
      'url(/images/' +
      (width ? '1080/back' : 'back') +
      randIndex +
      '-blur.jpg) no-repeat center center scroll';
    document.getElementById('hgroup').style.background =
      'url(/images/' +
      (width ? '1080/back' : 'back') +
      randIndex +
      '-blur.jpg) no-repeat center center scroll';
  }
}

// Event listeners
window.addEventListener('scroll', debounce(parallaxBlur, 10));
switchbutton.addEventListener('click', function () {
  switchBackground(true);
});

// Opens the right tab when loading the page
if (getAnchor() === 'contact') {
  removeActive();
  document.getElementById('tab-item-contact').classList.add('active');
  document.getElementById('contact-section').classList.add('active');
} else if (getAnchor() === 'resume') {
  removeActive();
  document.getElementById('tab-item-resume').classList.add('active');
  document.getElementById('resume-section').classList.add('active');
}

switchBackground(false);

// Tab navigation
document
  .getElementById('tab-item-about')
  .addEventListener('click', function () {
    removeActive();
    document.getElementById('tab-item-about').classList.add('active');
    document.getElementById('about-section').classList.add('active');
  });
document
  .getElementById('tab-item-resume')
  .addEventListener('click', function () {
    removeActive();
    document.getElementById('tab-item-resume').classList.add('active');
    document.getElementById('resume-section').classList.add('active');
  });
document
  .getElementById('tab-item-contact')
  .addEventListener('click', function () {
    removeActive();
    document.getElementById('tab-item-contact').classList.add('active');
    document.getElementById('contact-section').classList.add('active');
  });

// Form validation
let nameForm = document.getElementById('name-email-form');
let emailForm = document.getElementById('address-email-form');
let messageForm = document.getElementById('message-email-form');
let formElement = document.getElementById('form-email');

let regexEmail =
  /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g;

function check(element, event) {
  if (element.value == '') {
    if (event) {
      event.preventDefault();
    }
    element.classList.remove('is-success');
    element.classList.add('is-error');
  } else {
    element.classList.add('is-success');
    element.classList.remove('is-error');
  }
}

function checkEmail(element, event) {
  if (element.value == '' && !regexEmail.test(element.value)) {
    if (event) {
      event.preventDefault();
    }
    element.classList.remove('is-success');
    element.classList.add('is-error');
  } else {
    element.classList.add('is-success');
    element.classList.remove('is-error');
  }
}

nameForm.addEventListener('keyup', function () {
  check(nameForm, null);
});

emailForm.addEventListener('keyup', function () {
  checkEmail(emailForm, null);
});

messageForm.addEventListener('keyup', function () {
  check(messageForm, null);
});

formElement.addEventListener('submit', function (event) {
  check(nameForm, event);
  checkEmail(emailForm, event);
  check(messageForm, event);
});

function toggleDropdown() {
  const menu = document.querySelector('.dropdown-menu');
  const isExpanded = menu.classList.toggle('show');
  document
    .querySelector('.dropdown-toggle')
    .setAttribute('aria-expanded', isExpanded);
}

function downloadFile(language) {
  const fileUrl = `/path/to/${language}-file.zip`;
  const anchor = document.createElement('a');
  anchor.href = fileUrl;
  anchor.download = `${language}-file.zip`;
  anchor.click();
}

document.addEventListener('click', function (event) {
  if (!event.target.closest('.dropdown')) {
    document.querySelector('.dropdown-menu').classList.remove('show');
    document
      .querySelector('.dropdown-toggle')
      .setAttribute('aria-expanded', false);
  }
});
