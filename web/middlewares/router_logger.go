package middlewares

import (
	"encoding/json"
	"log"
	"net/http"
)

type logEntry struct {
	Method string `json:"method"`
	Route  string `json:"route"`
}

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		entry := logEntry{
			Method: r.Method,
			Route:  r.URL.Path,
		}

		logData, _ := json.Marshal(entry)
		log.Println(string(logData))

		next.ServeHTTP(w, r)
	})
}
