package internal

import (
	"embed"
	"html/template"
)

//go:embed template/*
var templateFS embed.FS

func GetIndexTemplate() (*template.Template, error) {
	tmpl, err := template.New("template").Funcs(template.FuncMap{
		"htmlSafe": func(html string) template.HTML {
			return template.HTML(html)
		},
		"len": func(array []string) int {
			return len(array)
		},
		"add": func(a, b int) int {
			return a + b
		},
	}).ParseFS(templateFS, "template/index/*")

	if err != nil {
		return nil, err
	}

	return tmpl, nil
}

func GetResumeTemplate() (*template.Template, error) {
	tmpl, err := template.New("template").Funcs(template.FuncMap{
		"htmlSafe": func(html string) template.HTML {
			return template.HTML(html)
		},
		"len": func(array []string) int {
			return len(array)
		},
		"add": func(a, b int) int {
			return a + b
		},
	}).ParseFS(templateFS, "template/resume/*")

	if err != nil {
		return nil, err
	}

	return tmpl, nil
}
